<project xmlns="http://maven.apache.org/POM/4.0.0" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    
	<modelVersion>4.0.0</modelVersion>
	
	<groupId>cz.cima.preclik</groupId>
	<artifactId>preclik</artifactId>
	<version>1.0.0-SNAPSHOT</version>
	
	<description>A demo implementation of performant e-shop warehouse and checkout backend.</description>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <pmd.plugin.failurePriority>2</pmd.plugin.failurePriority>
		<pmd.plugin.failurePriority-next>2</pmd.plugin.failurePriority-next>
		<java.version>13</java.version>
		<spring.boot.starter.version>2.3.2.RELEASE</spring.boot.starter.version>
		<spring.version>5.2.8.RELEASE</spring.version>
    </properties>
    
    <build>
    	<plugins>
    	   <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.8.1</version>
                <configuration>
                    <source>${java.version}</source>
                    <target>${java.version}</target>
                </configuration>
            </plugin>
	    	<plugin>
			    <groupId>org.openapitools</groupId>
			    <artifactId>openapi-generator-maven-plugin</artifactId>
			    <version>4.3.1</version>
			    <executions>
			        <execution>
			        	<id>warehouse</id>
			            <goals>
			                <goal>generate</goal>
			            </goals>
			            
			            <configuration>
			                <inputSpec>${project.basedir}/src/main/resources/api-warehouse.yaml</inputSpec>
			                <generatorName>spring</generatorName>
			                <generateSupportingFiles>false</generateSupportingFiles>
			                
			                <configOptions>
			                	 
			                	<basePackage>cz.cima.preclik.warehouse.web</basePackage>
			                	<apiPackage>cz.cima.preclik.warehouse.web.api</apiPackage>
			                	<invokerPackage>cz.cima.preclik.warehouse.web.api</invokerPackage>
			                	<modelPackage>cz.cima.preclik.warehouse.web.model</modelPackage>
				                <groupId>${project.groupId}</groupId>
				                <artifactId>${project.artifactId}</artifactId>
				                <artifactVersion>${project.version}</artifactVersion>
				                <interfaceOnly>true</interfaceOnly>
				                <skipDefaultInterface>true</skipDefaultInterface>
			                </configOptions>
			            </configuration>
			        </execution>
			    	<execution>
			    		<id>orders</id>
			            <goals>
			                <goal>generate</goal>
			            </goals>
			            
			            <configuration>
			                <inputSpec>${project.basedir}/src/main/resources/api-orders.yaml</inputSpec>
			                <generatorName>spring</generatorName>
			                <generateSupportingFiles>false</generateSupportingFiles>
			                
			                <configOptions>
			                	 
			                	<basePackage>cz.cima.preclik.orders.web</basePackage>
			                	<apiPackage>cz.cima.preclik.orders.web.api</apiPackage>
			                	<invokerPackage>cz.cima.preclik.orders.web.api</invokerPackage>
			                	<modelPackage>cz.cima.preclik.orders.web.model</modelPackage>
				                <groupId>${project.groupId}</groupId>
				                <artifactId>${project.artifactId}</artifactId>
				                <artifactVersion>${project.version}</artifactVersion>
				                <interfaceOnly>true</interfaceOnly>
				                <skipDefaultInterface>true</skipDefaultInterface>
			                </configOptions>
			            </configuration>
			        </execution>
			    </executions>
			</plugin>
			<plugin>
			    <groupId>org.codehaus.mojo</groupId>
			    <artifactId>build-helper-maven-plugin</artifactId>
			    <version>3.2.0</version>
			    <executions>
			        <execution>
			            <phase>generate-sources</phase>
			            <goals>
			                <goal>add-source</goal>
			            </goals>
			            <configuration>
			                <sources>
			                    <source>${project.build.directory}/generated-sources/openapi/src/main/java</source>
			                </sources>
			            </configuration>
			        </execution>
			    </executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-pmd-plugin</artifactId>
				<version>3.13.0</version>
				<executions>
					<execution>
						<phase>test</phase>
						<goals>
							<goal>check</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<rulesets>
	            		<ruleset>CimaJavaRuleset.xml</ruleset>
	            	</rulesets>
					<linkXref>true</linkXref>
					<sourceEncoding>utf-8</sourceEncoding>
					<minimumTokens>100</minimumTokens>
					<targetJdk>${java.version}</targetJdk>
					<printFailingErrors>true</printFailingErrors>
					<failurePriority>${pmd.plugin.failurePriority}</failurePriority>
				</configuration>
			    <dependencies>
			        <dependency>
			            <groupId>jp.skypencil</groupId>
			            <artifactId>RuleSet-for-SLF4J</artifactId>
			            <version>0.4</version>
			        </dependency>
			    </dependencies>
			</plugin>
			<plugin>
				<groupId>org.jacoco</groupId>
				<artifactId>jacoco-maven-plugin</artifactId>
				<version>0.8.5</version>
				<executions>
					<execution>
						<goals>
							<goal>prepare-agent</goal>
						</goals>
					</execution>
					<!-- attached to Maven test phase -->
					<execution>
						<id>report</id>
						<phase>test</phase>
						<goals>
							<goal>report</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
    	</plugins>
    </build>
    
    <dependencies>
        <dependency>
            <groupId>joda-time</groupId>
            <artifactId>joda-time</artifactId>
            <version>2.10.6</version>
        </dependency>

		<dependency>
		    <groupId>com.h2database</groupId>
		    <artifactId>h2</artifactId>
		    <version>1.4.200</version>
		    <scope>runtime</scope>
		</dependency>
		
		<dependency>
		    <groupId>org.slf4j</groupId>
		    <artifactId>slf4j-api</artifactId>
		    <version>1.7.30</version>
		</dependency>

        <!-- Validation -->
        <dependency>
            <groupId>javax.validation</groupId>
            <artifactId>validation-api</artifactId>
            <version>2.0.1.Final</version>
        </dependency>
        <dependency>
            <groupId>org.hibernate</groupId>
            <artifactId>hibernate-validator</artifactId>
            <version>6.1.5.Final</version>
        </dependency>
        
        <dependency>
            <groupId>org.hibernate.validator</groupId>
            <artifactId>hibernate-validator</artifactId>
            <version>6.1.2.Final</version>
        </dependency>

        <!-- Spring -->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot</artifactId>
            <version>${spring.boot.starter.version}</version>
        </dependency>
        <dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
			<version>${spring.boot.starter.version}</version>
		</dependency>
        	<dependency>
		    <groupId>org.springframework.boot</groupId>
		    <artifactId>spring-boot-starter-data-jpa</artifactId>
		    <version>${spring.boot.starter.version}</version>
		</dependency>
		<dependency>
		    <groupId>javax.xml.bind</groupId>
		    <artifactId>jaxb-api</artifactId>
		    <version>2.3.1</version>
		</dependency>
		<dependency>
		    <groupId>org.springframework</groupId>
		    <artifactId>spring-web</artifactId>
		    <version>${spring.version}</version>
		</dependency>
		
		<dependency>
		    <groupId>org.springframework.boot</groupId>
		    <artifactId>spring-boot-starter-security</artifactId>
		    <version>${spring.boot.starter.version}</version>
		</dependency>

		<dependency>
			<groupId>io.jsonwebtoken</groupId>
			<artifactId>jjwt</artifactId>
			<version>0.9.1</version>
		</dependency>
		
		<dependency>
		    <groupId>com.fasterxml.jackson.core</groupId>
		    <artifactId>jackson-databind</artifactId>
		    <version>2.11.1</version>
		</dependency>
		       

		<!-- openapi -->        
        <dependency>
		    <groupId>com.fasterxml.jackson.core</groupId>
		    <artifactId>jackson-annotations</artifactId>
		    <version>2.11.1</version>
		</dependency>

		<dependency>
		    <groupId>io.swagger</groupId>
		    <artifactId>swagger-annotations</artifactId>
		    <version>1.6.2</version>
		</dependency>
		
		<dependency>
		    <groupId>org.openapitools</groupId>
		    <artifactId>jackson-databind-nullable</artifactId>
		    <version>0.2.1</version>
		</dependency>



        <dependency>
            <groupId>org.apache.httpcomponents</groupId>
            <artifactId>httpclient</artifactId>
            <version>4.5.12</version>
        </dependency>


        <dependency>
            <groupId>org.apache.httpcomponents</groupId>
            <artifactId>fluent-hc</artifactId>
            <version>4.5.12</version>
        </dependency>

        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
            <version>3.11</version>
        </dependency>
        
        <dependency>
		    <groupId>org.apache.commons</groupId>
		    <artifactId>commons-collections4</artifactId>
		    <version>4.4</version>
		</dependency>

        <!-- TEST -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.13</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-core</artifactId>
            <version>3.4.4</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <version>${spring.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <version>${spring.boot.starter.version}</version>
            <scope>test</scope>
        </dependency>
        
        <!-- TODO: tohle asi vyhod�me a uvid�me co dot�hne openapi maven plugin -->
        <dependency>
            <groupId>jakarta.annotation</groupId>
            <artifactId>jakarta.annotation-api</artifactId>
            <version>1.3.5</version>
            <scope>compile</scope>
        </dependency>
    </dependencies>
</project>
