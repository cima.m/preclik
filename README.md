An example implementation of selfcontained simple webshop in spring.

 ![alt text](Doc/preclik-overview.svg "Overview of object design of the shop.")
 
 # TODO
 - Orders
   - API
   - Submit new Order/Item
   - Cancel Item (Order) 
 - Expiration watchdog
   - Obsidian scheduler
 - Prometheus
