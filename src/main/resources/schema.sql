DROP TABLE IF EXISTS PRODUCTS;
  
CREATE TABLE PRODUCTS (
  id VARCHAR(40) PRIMARY KEY,

  name VARCHAR(250) NOT NULL,
  modified_timestamp TIMESTAMP NOT NULL,
  
  description CLOB NOT NULL,
  unit_type VARCHAR(32) NOT NULL,
  unit_price DECIMAL NOT NULL,
  gallery VARCHAR(256),
  actual_amount INT,
  available_amount INT
);

DROP TABLE IF EXISTS ORDERS;
  
CREATE TABLE ORDERS (
  id VARCHAR(40) PRIMARY KEY,

  user_name VARCHAR(250) NOT NULL,
  modified_timestamp TIMESTAMP NOT NULL,
  
  finalized BOOLEAN NOT NULL,
  canceled BOOLEAN NOT NULL
);

DROP TABLE IF EXISTS ORDER_ITEMS;
  
CREATE TABLE ORDER_ITEMS (
  id VARCHAR(40) PRIMARY KEY,
  order_id VARCHAR(40) NOT NULL,
  product_id VARCHAR(40) NOT NULL,
  amount INT,
  foreign key (order_id) references ORDERS(id),
  foreign key (product_id) references PRODUCTS(id)
);