package cz.cima.preclik;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * An entry point for spring boot application 
 * when run as ordinary JVM application 
 * instead of running it by Maven.
 */
@SpringBootApplication
public class PreclikApplication {
	
	/**
	 * Entry point of spring boot application when run as classical
	 * Java application instead of run by maven or spring boot scripts.
	 * 
	 * NOTE: Never rely this method gets called. 
	 * It is not spring philosophy to run spring like this.
	 * 
	 * @param args command line arguments to be passed 
	 * to a spring boot application during start.
	 */
	public static void main(String[] args) {
		SpringApplication.run(PreclikApplication.class, args);
	}
}