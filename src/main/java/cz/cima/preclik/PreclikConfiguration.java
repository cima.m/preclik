package cz.cima.preclik;

import java.time.Clock;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Most common beans are defined here
 * Other services do share them.
 */
@Configuration
public class PreclikConfiguration {
	
	/**
	 * @return default system UTC clock to be used by other beans 
	 * if working with time is needed.
	 */
	@Bean
	public Clock getClock() {
		return Clock.systemUTC();
	}
}
