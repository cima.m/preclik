package cz.cima.preclik.orders.data.model;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * ORM class for Order entity.
 *
 */
@Entity(name="Order")
@Table(name="ORDERS")
@SuppressWarnings("PMD.DataClass")
public class OrderEntity {
	
	@Id
	@Column(name="id")
	private String orderId;

	@Column(name="user_name")
	private String userName;
	
	@Column(name="modified_timestamp")
	private Timestamp timestamp;
	
	@Column(name="finalized")
	private boolean finalized;
	
	@Column(name="canceled")
	private boolean canceled;
	
	@OneToMany
	@JoinColumn(name = "order_id")
	private List<ProductItemEntity> items;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	public boolean isFinalized() {
		return finalized;
	}

	public void setFinalized(boolean finalized) {
		this.finalized = finalized;
	}

	public boolean isCanceled() {
		return canceled;
	}

	public void setCanceled(boolean canceled) {
		this.canceled = canceled;
	}

	public List<ProductItemEntity> getItems() {
		return items;
	}

	public void setItems(List<ProductItemEntity> items) {
		this.items = items;
	}
}
