package cz.cima.preclik.orders.data;

import org.springframework.data.repository.CrudRepository;

import cz.cima.preclik.orders.data.model.ProductItemEntity;

/**
 * JPA based automatic Data Access Object (DAO) for persisting Order entities in database.
 */
public interface ProductItemRepository extends CrudRepository<ProductItemEntity, String> {
}
