package cz.cima.preclik.orders.data.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * ORM class for product item entity.
 *
 */
@Entity(name="ProductItemEntity")
@Table(name="ORDER_ITEMS")
@SuppressWarnings("PMD.DataClass")
public class ProductItemEntity {
	
	@Id
	@Column(name="id")
	private String productItemId;
	
	@Column(name="order_id")
	private String orderId;
	
	@Column(name="product_id")
	private String productId;

	@Column(name="amount")
	private int amount;

	public String getProductItemId() {
		return productItemId;
	}

	public void setProductItemId(String productItemId) {
		this.productItemId = productItemId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
	
}