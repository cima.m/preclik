package cz.cima.preclik.orders.data;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Persistence layer configuration.
 * Currently just an anchor to use EnableJpaRepositories annotation.
 */
@Configuration
@EnableJpaRepositories
public class OrdersRepositoryConfiguration {
	
}
