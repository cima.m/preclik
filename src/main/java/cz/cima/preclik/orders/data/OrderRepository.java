package cz.cima.preclik.orders.data;

import org.springframework.data.repository.CrudRepository;

import cz.cima.preclik.orders.data.model.OrderEntity;


/**
 * JPA based automatic Data Access Object (DAO) for persisting Order entities in database.
 */
public interface OrderRepository extends CrudRepository<OrderEntity, String> {
	
	/**
	 * @param userName - for whom to find the latest non-finalized Order
	 * @return newest non-finalized order for given user
	 */
	OrderEntity findFirstByUserNameAndFinalizedFalseOrderByTimestampDesc(String userName);

	/**
	 * @param userName - for whom to find all orders
	 * @return all orders for given user.
	 */
	Iterable<OrderEntity> findAllByUserNameOrderByTimestampDesc(String userName);

	/**
	 * @param orderId which order to fetch
	 * @param username for whom
	 * @return Order entity with given ID if user is same as tha claimed one.
	 */
	OrderEntity findByOrderIdAndUserName(String orderId, String username);
}
