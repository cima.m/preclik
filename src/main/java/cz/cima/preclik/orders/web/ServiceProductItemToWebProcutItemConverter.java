package cz.cima.preclik.orders.web;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import cz.cima.preclik.orders.web.model.ProductItem;

/**
 * Service layer product item to web layer.
 *
 */
@Component
public class ServiceProductItemToWebProcutItemConverter implements Converter<cz.cima.preclik.orders.service.model.ProductItem, ProductItem> {

	@Override
	public ProductItem convert(cz.cima.preclik.orders.service.model.ProductItem source) {
		ProductItem productItem = new ProductItem();
		productItem.setProductId(source.getProductId());
		productItem.setAmount(source.getAmount());
		return productItem;
	}

}
