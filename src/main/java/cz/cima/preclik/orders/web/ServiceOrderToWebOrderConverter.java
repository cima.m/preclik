package cz.cima.preclik.orders.web;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import cz.cima.preclik.orders.web.model.Order;
import cz.cima.preclik.orders.web.model.OrderState;
import cz.cima.preclik.orders.web.model.ProductItem;

/**
 * From service layer to api layer.
 *
 */
@Component
public class ServiceOrderToWebOrderConverter implements Converter<cz.cima.preclik.orders.service.model.Order, Order> {

	@Autowired
	private ServiceProductItemToWebProcutItemConverter conversionService;

	@Override
	public Order convert(cz.cima.preclik.orders.service.model.Order serviceOrder) {
		Order webOrder = new Order();
		
		webOrder.setId(serviceOrder.getOrderId());
		webOrder.setTimestamp(serviceOrder.getTimestamp().toString());
		OrderState state = new OrderState();
		state.setFinalized(serviceOrder.isFinalized());
		state.setCanceled(serviceOrder.isCanceled());
		webOrder.setState(state);
		
		List<cz.cima.preclik.orders.service.model.ProductItem> productItems = serviceOrder.getItems();
		List<ProductItem> newItems = new LinkedList<>();
		for (var productItem : productItems) {
			newItems.add(conversionService.convert(productItem));	
		}
		
		webOrder.setProductItems(newItems);
		
		return webOrder;
	}

}
