package cz.cima.preclik.orders.web;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;

import cz.cima.preclik.orders.service.AccessViolationException;
import cz.cima.preclik.orders.service.NotFoundExcption;
import cz.cima.preclik.orders.service.OrdersService;
import cz.cima.preclik.orders.web.api.OrdersApi;
import cz.cima.preclik.orders.web.model.Order;
import cz.cima.preclik.orders.web.model.OrderState;
import cz.cima.preclik.orders.web.model.ProductItem;

/**
 * REST API controller for user side part of the web shop.
 * Creates, lists and manages orders and items within them. 
 * API is user-centric. Each call assumes logged in user. 
 *
 */
@Controller
@RequestMapping("v1")
@PreAuthorize("hasRole('USER')")
public class OrdersController implements OrdersApi {

	private static final Logger LOG = LoggerFactory.getLogger(OrdersController.class);

	@Autowired
	private OrdersService ordersService;
	
	@Autowired
	private ConversionService conversionService;
	
	@Override
	public ResponseEntity<Order> addItem(
			@Pattern(regexp = "[0-9a-fA-F]{8}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{12}") String orderId,
			@Valid ProductItem productItem) {
		
		
		String username = getCurrentUserName();
		try {
			var order = ordersService.addItem(username, orderId, productItem.getProductId(), productItem.getAmount());
			
			LOG.debug("{}", order);
						
			return ResponseEntity.ok(conversionService.convert(order, Order.class));	
		} catch (AccessViolationException e) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Insert into other user's order.", e);
		}
		
		
	}

	@Override
	public ResponseEntity<Order> finalizeOrder(
			@Pattern(regexp = "[0-9a-fA-F]{8}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{12}") String orderId,
			@Valid OrderState orderState) {
		
		try {
			String username = getCurrentUserName();
			var serviceOrder = ordersService.finalizeOrder(username, orderId, orderState.getCanceled());
			return ResponseEntity.ok(conversionService.convert(serviceOrder, Order.class));
		} catch (NotFoundExcption e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User have no such order.", e);
		}
	}

	@Override
	public ResponseEntity<Order> getActiveOrder() {
		try {
			String username = getCurrentUserName();
			var activeServiceOrder = ordersService.getActiveOrderForUser(username);
			LOG.debug("Active order ID {} for user {}", activeServiceOrder.getOrderId(), username);
			
			return ResponseEntity.ok(conversionService.convert(activeServiceOrder, Order.class)); 
		} catch (NotFoundExcption e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No active order in database", e);
		}
	}

	@Override
	public ResponseEntity<Order> getOrder(
			@Pattern(regexp = "[0-9a-fA-F]{8}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{12}") String orderId) {
		String username = getCurrentUserName();
		try {
			var serviceOrder = ordersService.getOrderForUser(username, orderId);
			return ResponseEntity.ok(conversionService.convert(serviceOrder, Order.class));
		} catch (NotFoundExcption e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User have no such order.", e);
		}
	}

	@Override
	public ResponseEntity<List<Order>> listOrders() {
		String username = getCurrentUserName();
		List<cz.cima.preclik.orders.service.model.Order> listOrdersForUser = ordersService.listOrdersForUser(username);
		List<Order> webOrders = new LinkedList<>();
		for (var order : listOrdersForUser) {
			webOrders.add(conversionService.convert(order, Order.class));
		}

		return ResponseEntity.ok(webOrders);
	}
	
	private String getCurrentUserName() throws ResponseStatusException {
		
		final SecurityContext context = SecurityContextHolder.getContext();
		if(Objects.isNull(context)) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		}
		
		final Authentication authentication = context.getAuthentication();
		if(Objects.isNull(authentication)) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		}
		
		final Object userPrincipal = authentication.getPrincipal();
		if(userPrincipal instanceof UserDetails) {
			var user = (UserDetails)userPrincipal;
			String username = user.getUsername();
			if(StringUtils.isNotBlank(username)) {
				return username;
			}
		}
		throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
	}

}
