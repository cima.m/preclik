package cz.cima.preclik.orders.service;

/**
 * Tells catcher that requested combination would violate authorization.
 *
 */
public class AccessViolationException extends Exception {

	private static final long serialVersionUID = 1268149430195339751L;

}
