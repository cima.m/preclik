package cz.cima.preclik.orders.service.model;

import java.util.List;

import org.joda.time.DateTime;

/**
 * Service layer Order representation.
 *
 */
@SuppressWarnings("PMD.DataClass")
public class Order {
	
	private String orderId;
	private String userName;
	private DateTime timestamp;
	private boolean finalized;
	private boolean canceled;

	private List<ProductItem> items;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public DateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(DateTime timestamp) {
		this.timestamp = timestamp;
	}

	public boolean isFinalized() {
		return finalized;
	}

	public void setFinalized(boolean finalized) {
		this.finalized = finalized;
	}

	public boolean isCanceled() {
		return canceled;
	}

	public void setCanceled(boolean canceled) {
		this.canceled = canceled;
	}

	public List<ProductItem> getItems() {
		return items;
	}

	public void setItems(List<ProductItem> items) {
		this.items = items;
	}

}
