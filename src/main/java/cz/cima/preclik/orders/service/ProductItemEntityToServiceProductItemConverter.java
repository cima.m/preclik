package cz.cima.preclik.orders.service;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import cz.cima.preclik.orders.data.model.ProductItemEntity;
import cz.cima.preclik.orders.service.model.ProductItem;

/**
 * Converts product item from data layer to service layer representation.
 *
 */
@Component
public class ProductItemEntityToServiceProductItemConverter implements Converter<ProductItemEntity, ProductItem> {

	@Override
	public ProductItem convert(ProductItemEntity productItemEntity) {
		ProductItem productItem = new ProductItem();
		productItem.setProductItemId(productItemEntity.getProductItemId());
		productItem.setOrderId(productItemEntity.getOrderId());
		productItem.setProductId(productItemEntity.getProductId());
		productItem.setAmount(productItemEntity.getAmount());
		return productItem;
	}

}
