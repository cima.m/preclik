package cz.cima.preclik.orders.service;

/**
 * Exception evoking that requested entity wasn't found.
 *
 */
public class NotFoundExcption extends Exception {

	private static final long serialVersionUID = 7755313883459250299L;

}
