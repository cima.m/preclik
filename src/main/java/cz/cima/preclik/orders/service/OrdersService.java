package cz.cima.preclik.orders.service;

import java.sql.Timestamp;
import java.time.Clock;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import javax.transaction.Transactional;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import cz.cima.preclik.orders.data.OrderRepository;
import cz.cima.preclik.orders.data.ProductItemRepository;
import cz.cima.preclik.orders.data.model.OrderEntity;
import cz.cima.preclik.orders.data.model.ProductItemEntity;
import cz.cima.preclik.orders.service.model.Order;

/**
 * Service layer mediator for user ordering system. 
 *
 */
@Service
public class OrdersService {

	private static final Logger LOG = LoggerFactory.getLogger(OrdersService.class);
	
	@Autowired
	private OrderRepository ordersRepository;
	
	@Autowired
	private ProductItemRepository itemRepository;
	
	@Autowired
	private ConversionService conversionService;
	
	private final Clock clock;
	
	/**
	 * @param clock to provide source of time to part of the code that includes timestamp.
	 */
	@Autowired
	public OrdersService(Clock clock) {
		this.clock = clock;
	}
	
	public Order getActiveOrderForUser(String username) throws NotFoundExcption {

		if(StringUtils.isNotBlank(username)) {
			OrderEntity order = ordersRepository.findFirstByUserNameAndFinalizedFalseOrderByTimestampDesc(username);
			LOG.debug("{}", order);
			
			if(Objects.nonNull(order)){
				return conversionService.convert(order, Order.class);
			}
		}
		
		throw new NotFoundExcption();
	}

	@Transactional
	public Order addItem(String username, String orderId, String productId, int amount) throws AccessViolationException {

		Optional<OrderEntity> existingOrder = ordersRepository.findById(orderId);
		OrderEntity orderEntity = null;
		if(Objects.isNull(existingOrder) || ! existingOrder.isPresent()) {
			//create new order
			OrderEntity newOrderEntity = new OrderEntity();
			newOrderEntity.setOrderId(orderId);
			newOrderEntity.setUserName(username);
			newOrderEntity.setTimestamp(Timestamp.from(clock.instant()));
			newOrderEntity.setFinalized(false);
			newOrderEntity.setCanceled(false);
			
			orderEntity = ordersRepository.save(newOrderEntity);
		}else if( ! StringUtils.equals(username, existingOrder.get().getUserName())) {
			throw new AccessViolationException();
		} else {
			orderEntity = existingOrder.get();
		}

		//insert item
		ProductItemEntity itemEntity = new ProductItemEntity();
		
		itemEntity.setProductItemId(UUID.randomUUID().toString());
		itemEntity.setOrderId(orderEntity.getOrderId());
		itemEntity.setProductId(productId);
		itemEntity.setAmount(amount);
		
		itemRepository.save(itemEntity);
		
		Optional<OrderEntity> newOrder = ordersRepository.findById(orderId);
		
		LOG.debug("{}", newOrder.get());
		
		return conversionService.convert(newOrder.get(), Order.class);
		
		
	}

	public List<Order> listOrdersForUser(String username) {
		Iterable<OrderEntity> userOrders = ordersRepository.findAllByUserNameOrderByTimestampDesc(username);
		List<Order> orders = new LinkedList<>();
		for (OrderEntity orderEntity : userOrders) {
			orders.add(conversionService.convert(orderEntity, Order.class));
		}
		
		return orders;
	}

	public Order getOrderForUser(String username, String orderId) throws NotFoundExcption {
		OrderEntity findByIdAndUserName = ordersRepository.findByOrderIdAndUserName(orderId, username);
		if(Objects.isNull(findByIdAndUserName)) {
			throw new NotFoundExcption();
		}
		return conversionService.convert(findByIdAndUserName, Order.class);
	}

	@Transactional
	public Order finalizeOrder(String username, String orderId, boolean canceled) throws NotFoundExcption {
		
		OrderEntity orderEntity = ordersRepository.findByOrderIdAndUserName(orderId, username);
		if(Objects.isNull(orderEntity)) {
			throw new NotFoundExcption();
		}
		
		orderEntity.setFinalized(true);
		orderEntity.setCanceled(canceled);
		
		ordersRepository.save(orderEntity);
		
		return conversionService.convert(orderEntity, Order.class);
	}

}
