package cz.cima.preclik.orders.service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.ObjectUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import cz.cima.preclik.orders.data.model.OrderEntity;
import cz.cima.preclik.orders.data.model.ProductItemEntity;
import cz.cima.preclik.orders.service.model.Order;
import cz.cima.preclik.orders.service.model.ProductItem;

/**
 * Converts Database entities into servicelayer Order POJO.
 *
 */
@Component
public class OrderEntityToServiceEntityConverter implements Converter<OrderEntity, Order> {

	private static final List<ProductItemEntity> EMPTY_ITEMS = Collections.emptyList();
	
	@Autowired
	private ConversionService conversionService;
	
	
	@Override
	public Order convert(OrderEntity orderEntity) {
		Order order = new Order();
		
		order.setOrderId(orderEntity.getOrderId());
		order.setUserName(orderEntity.getUserName());
		order.setTimestamp(new DateTime(orderEntity.getTimestamp().getTime()));
		order.setFinalized(orderEntity.isFinalized());
		order.setCanceled(orderEntity.isCanceled());
		
		List<ProductItem> serviceProductItems = ObjectUtils.firstNonNull(orderEntity.getItems(), EMPTY_ITEMS).stream().map(
					productItemEntity -> conversionService.convert(productItemEntity, ProductItem.class)	
		).collect(Collectors.toList());

		order.setItems(serviceProductItems);
				
		return order;
	}

}
