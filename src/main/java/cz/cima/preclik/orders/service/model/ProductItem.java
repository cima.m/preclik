package cz.cima.preclik.orders.service.model;

/**
 * Service layer shopping cart item representation.
 *
 */
@SuppressWarnings("PMD.DataClass")
public class ProductItem {
	
	private String productItemId;
	private String orderId;	
	private String productId;
	private int amount;
	
	public String getProductItemId() {
		return productItemId;
	}
	public void setProductItemId(String productItemId) {
		this.productItemId = productItemId;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	
}
