package cz.cima.preclik.authorization;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {

	private static final Logger LOG = LoggerFactory.getLogger(JwtRequestFilter.class);
	
	private static final String BEARER_PREFIX = "Bearer ";
	
	@Value("${io.jwt.signingKey}")
	private String secret;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {

		final String requestTokenHeader = request.getHeader("Authorization");

		String username = null;
		String jwtToken = null;

		if (StringUtils.startsWith(requestTokenHeader, BEARER_PREFIX)) {

			jwtToken = requestTokenHeader.substring(BEARER_PREFIX.length());
	
			try {
				username = getUsernameFromToken(jwtToken);
			} catch (IllegalArgumentException e) {
				LOG.warn("Unable to get JWT Token");
			}

		} else {
			LOG.warn("JWT Token does not begin with Bearer String");
		}

		if (StringUtils.isNotBlank(username) 
				&& SecurityContextHolder.getContext().getAuthentication() == null) {

			Collection<? extends GrantedAuthority> authorities = getAuthoritiesFromToken(jwtToken);			
			final UserDetails userDetails = new User(getUsernameFromToken(jwtToken), Strings.EMPTY, authorities);
			final UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
			
			SecurityContextHolder.getContext().setAuthentication(authToken);
		}

		filterChain.doFilter(request, response);
	}
	
	private String getUsernameFromToken(String token) {
		return getClaimFromToken(token, Claims::getSubject);
	}

	private Collection<GrantedAuthority> getAuthoritiesFromToken(String token) {
		return getClaimFromToken(token, claims -> getRoles(claims)).stream().map(role -> new SimpleGrantedAuthority("ROLE_" + role)).collect(Collectors.toList());
	}
	
	@SuppressWarnings("unchecked")
	private static Collection<String> getRoles(Claims claims){
		return (Collection<String>)claims.get("roles", Collection.class);
	}
	
	private <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
		final Claims claims = getAllClaimsFromToken(token);
		return claimsResolver.apply(claims);

	}

	private Claims getAllClaimsFromToken(String token) {
		return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
	}

}