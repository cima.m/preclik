package cz.cima.preclik.warehouse.web;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import static org.apache.commons.collections4.CollectionUtils.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import cz.cima.preclik.warehouse.service.MalformedProductException;
import cz.cima.preclik.warehouse.service.NoSuchProductException;
import cz.cima.preclik.warehouse.service.ProductCreationException;
import cz.cima.preclik.warehouse.service.ProductRepositoryException;
import cz.cima.preclik.warehouse.service.ProductService;
import cz.cima.preclik.warehouse.web.api.ProductsApi;
import cz.cima.preclik.warehouse.web.model.Product;
import cz.cima.preclik.warehouse.web.model.ProductDescription;

/**
 * An API implementation for warehouse module.
 * Everything is based on OpenAPI definition stored in resources.
 */
@Controller
@RequestMapping("v1")
public class WarehouseControler implements  ProductsApi {
	
	private static final Logger LOG = LoggerFactory.getLogger(WarehouseControler.class);

	@Autowired
	private ProductService productService;
	
	@Autowired
	private ConversionService conversionService;
	

	@Override
	@SuppressWarnings("PMD.DataflowAnomalyAnalysis")
	@PreAuthorize("hasRole('BACKOFFICE_OPERATOR')")
	public ResponseEntity<Product> createProduct(@Valid ProductDescription productDescription) {
		
		var serviceProduct = conversionService.convert(productDescription, cz.cima.preclik.warehouse.service.model.ProductDescription.class);
		
		ResponseEntity<Product> response;
		
		try {
			var resultServiceProduct = productService.createProduct(serviceProduct);
			response = ResponseEntity.ok(conversionService.convert(resultServiceProduct, Product.class));
			
		} catch (ProductCreationException pce) {
			response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
			
		} catch (MalformedProductException mpe) {
			response = ResponseEntity.badRequest().build();
		}
		
		return response;
		
	}

	
	@Override
	@SuppressWarnings("PMD.DataflowAnomalyAnalysis")
	public ResponseEntity<List<Product>> listProducts() {
		
		LOG.debug("{}", SecurityContextHolder.getContext().getAuthentication().getAuthorities());
		
		ResponseEntity<List<Product>> response;
		
		try {
			var productCollection = productService.listProducts();
			final List<Product> entities;
			if(isNotEmpty(productCollection)) {
				entities = productCollection.stream()
							.map(item -> conversionService.convert(item, Product.class))
							.collect(Collectors.toList());
			} else {
				entities = Collections.emptyList();
			}
			
			response = ResponseEntity.ok(entities);
			
		} catch (ProductRepositoryException e) {
			response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
		
		return response;
	}

	@Override
	@SuppressWarnings("PMD.DataflowAnomalyAnalysis")
	@PreAuthorize("hasRole('BACKOFFICE_OPERATOR')")
	public ResponseEntity<Product> patchProduct(String productId, @Valid Product product) {
		LOG.debug("{} -> {}", productId, product);
				
		var serviceProduct = conversionService.convert(product, cz.cima.preclik.warehouse.service.model.Product.class);
		serviceProduct.setId(productId);
		
		ResponseEntity<Product> response;
		try {
			var newSeviceProduct = productService.patchProduct(serviceProduct);
			Product newWebProduct = conversionService.convert(newSeviceProduct, Product.class);
			response = ResponseEntity.ok(newWebProduct);
			
		} catch (MalformedProductException e) {
			response = ResponseEntity.badRequest().build();
			
		} catch (NoSuchProductException e) {
			response = ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		
		return response;
	}
	
}
