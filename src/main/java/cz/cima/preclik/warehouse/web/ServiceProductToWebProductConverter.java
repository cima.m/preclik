package cz.cima.preclik.warehouse.web;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.time.ZoneId;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import cz.cima.preclik.warehouse.web.model.Product;

/**
 * Spring compatible converter of Product representation from Service layer to API object.
 * @implNote It only creates shallow copy.
 */
@Component
public class ServiceProductToWebProductConverter implements Converter<cz.cima.preclik.warehouse.service.model.Product, Product> {

	/**
	 * Converts Service layer Product structure to Web layer product structure.
	 * 
	 * @param serviceProduct Service layer structure whose attributes 
	 *        to be reorganized into Web layer Product structure.
	 *        
	 * @return Web layer Product structure with members populated by values
	 * 		   provided in Service layer product structure 
	 *         form parameter. Shallow copy.
	 */
	@Override
	public Product convert(cz.cima.preclik.warehouse.service.model.Product serviceProduct) {
		
		if(ObjectUtils.isEmpty(serviceProduct)) {
			throw new IllegalArgumentException("Null Service product can't be converted to a Web Product");
		}
		
		var webProduct = new Product();
		
		webProduct.setId(serviceProduct.getId());
		
		webProduct.setTimestamp(OffsetDateTime.ofInstant(
					java.time.Instant.ofEpochMilli(serviceProduct.getTimestamp().getMillis()), 
					ZoneId.of(serviceProduct.getTimestamp().getZone().getID())
			));
		
		webProduct.setName(serviceProduct.getName());
		webProduct.setDescription(serviceProduct.getDescription());
		webProduct.setUnitType(serviceProduct.getUnitType());
		webProduct.setUnitPrice(BigDecimal.valueOf(serviceProduct.getUnitPrice()));
		webProduct.setGallery(serviceProduct.getGallery());
		webProduct.setActualAmount(serviceProduct.getActualAmount());
		webProduct.setAvailableAmount(serviceProduct.getAvailableAmount());		
		
		return webProduct;
	}
}
