package cz.cima.preclik.warehouse.web;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import cz.cima.preclik.warehouse.web.model.ProductDescription;

/**
 * Converts API layer Product Description to Service layer Product description object.
 */
@Component
public class WebProdutDescriptionToServiceProductDesctription implements Converter<ProductDescription, cz.cima.preclik.warehouse.service.model.ProductDescription> {

	@Override
	public cz.cima.preclik.warehouse.service.model.ProductDescription convert(ProductDescription webProductDesc) {
		var serviceProduct = new cz.cima.preclik.warehouse.service.model.ProductDescription();

		serviceProduct.setName(webProductDesc.getName());
		serviceProduct.setDescription(webProductDesc.getDescription());
		serviceProduct.setUnitType(webProductDesc.getUnitType());
		serviceProduct.setUnitPrice(webProductDesc.getUnitPrice().doubleValue());
		serviceProduct.setGallery(webProductDesc.getGallery());
		
		return serviceProduct;
	}

}
