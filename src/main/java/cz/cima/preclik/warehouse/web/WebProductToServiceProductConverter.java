package cz.cima.preclik.warehouse.web;

import java.util.Objects;

import org.joda.time.DateTime;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import cz.cima.preclik.warehouse.web.model.Product;


/**
 * Product structure converter service between web and service layers.
 * */
@Component
public class WebProductToServiceProductConverter implements 
			Converter<Product, cz.cima.preclik.warehouse.service.model.Product> {

	/**
	 * Converts Web layer Product structure to Service layer Product structure.
	 * 
	 * @param webProduct Web layer Product structure whose members to be reorganized into Service layer Product structure.
	 * @return Service layer Product structure populated by values provided in web layer product given in parameter. Shallow copy.
	 */
	@Override
	public cz.cima.preclik.warehouse.service.model.Product convert(Product webProduct) {
		
		var serviceProduct = new cz.cima.preclik.warehouse.service.model.Product();
		
		serviceProduct.setId(webProduct.getId());
		
		if(Objects.nonNull(webProduct.getTimestamp())) {
			serviceProduct.setTimestamp(new DateTime(webProduct.getTimestamp().toEpochSecond() * 1000 ));
		}
		
		serviceProduct.setName(webProduct.getName());
		serviceProduct.setDescription(webProduct.getDescription());
		serviceProduct.setUnitType(webProduct.getUnitType());
		
		if(Objects.nonNull(webProduct.getUnitPrice())) {
			serviceProduct.setUnitPrice(webProduct.getUnitPrice().doubleValue());
		}
		
		serviceProduct.setGallery(webProduct.getGallery());
		
		if(Objects.nonNull(webProduct.getActualAmount())) {
			serviceProduct.setActualAmount(webProduct.getActualAmount());
		}
		
		if(Objects.nonNull(webProduct.getAvailableAmount())) {
			serviceProduct.setAvailableAmount(webProduct.getAvailableAmount());
		}
		
		return serviceProduct;
	}
}
