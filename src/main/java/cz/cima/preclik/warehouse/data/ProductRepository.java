package cz.cima.preclik.warehouse.data;

import org.springframework.data.repository.CrudRepository;

import cz.cima.preclik.warehouse.data.model.ProductEntity;

/**
 * JPA based automatic Data Access Object (DAO) for persisting product entities in database.
 */
public interface ProductRepository extends CrudRepository<ProductEntity, String> {
}
