package cz.cima.preclik.warehouse.data.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import java.sql.Timestamp;

/**
 * An Object ORM counterpart to database entity 
 * representing product offered by this web shop. 
 *
 */
@Entity(name="Product")
@Table(name="PRODUCTS")
@SuppressWarnings("PMD.DataClass")
public class ProductEntity {

	@Id
	@Column(name="id")
	private String productId;

	@Column(name="name")
	private String name;
	
	@Column(name="modified_timestamp")
	private Timestamp timestamp;
	
	@Column(name="description")
	private String description;
	
	
	@Column(name="unit_type")
	private String unitType;
	
	
	@Column(name="unit_price")
	private double unitPrice;
	
	@Column(name="gallery")
	private String gallery;
	
	@Column(name="actual_amount")
	private int actualAmount;
	
	@Column(name="available_amount")
	private int availableAmount;

	/**
	 * @return unique identifier of product represented by this object within system.
	 */
	public String getId() {
		return productId;
	}

	/**
	 * Sets given value as a unique identifier of product represented by this object.
	 * 
	 * @param productId string to be used to uniquely identify product represented by this object within system. 
	 */
	public void setId(String productId) {
		this.productId = productId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUnitType() {
		return unitType;
	}

	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}

	public double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public String getGallery() {
		return gallery;
	}

	public void setGallery(String gallery) {
		this.gallery = gallery;
	}

	public int getActualAmount() {
		return actualAmount;
	}

	public void setActualAmount(int actualAmount) {
		this.actualAmount = actualAmount;
	}

	public int getAvailableAmount() {
		return availableAmount;
	}

	public void setAvailableAmount(int availableAmount) {
		this.availableAmount = availableAmount;
	}
	
}
