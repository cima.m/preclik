package cz.cima.preclik.warehouse.service;

/**
 * Exception denoting that Product to be processed doesn't exists in database.
 *
 */
public class NoSuchProductException extends Exception {

	private static final long serialVersionUID = 8430805298363338693L;

}
