package cz.cima.preclik.warehouse.service;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Objects;

import org.apache.commons.lang3.ObjectUtils;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import cz.cima.preclik.warehouse.data.model.ProductEntity;
import cz.cima.preclik.warehouse.service.model.Product;

/**
 * Spring compatible converter of Product representation from Service layer to Database entity.
 * @implNote It only creates shallow copy.
 */
@Component
public class ServiceProductToProductEntityConverter implements Converter<Product, ProductEntity> {

	@Override
	public ProductEntity convert(Product serviceProduct) {
		
		if(ObjectUtils.isEmpty(serviceProduct)) {
			throw new IllegalArgumentException("Cannot convert Null service Product object to entity");
		}
		
		ProductEntity productEntity = new ProductEntity();
		productEntity.setId(serviceProduct.getId());
		productEntity.setName(serviceProduct.getName());
		
		if(Objects.nonNull(serviceProduct.getTimestamp())) {
			productEntity.setTimestamp(Timestamp.from(Instant.ofEpochMilli(serviceProduct.getTimestamp().getMillis())));
		}

		productEntity.setDescription(serviceProduct.getDescription());
		productEntity.setUnitType(serviceProduct.getUnitType());
		productEntity.setUnitPrice(serviceProduct.getUnitPrice());
		productEntity.setGallery(serviceProduct.getGallery());
		productEntity.setActualAmount(serviceProduct.getActualAmount());
		productEntity.setAvailableAmount(serviceProduct.getAvailableAmount());
		
		return productEntity;
	}

}
