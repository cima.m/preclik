package cz.cima.preclik.warehouse.service;

import org.apache.commons.lang3.ObjectUtils;
import org.joda.time.DateTime;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import cz.cima.preclik.warehouse.data.model.ProductEntity;
import cz.cima.preclik.warehouse.service.model.Product;

/**
 * Spring compatible converter of Product representation from Database form to Service layer object.
 * @implNote It only creates shallow copy.
 */
@Component
public class ProductEntityToServiceProductConverter implements Converter<ProductEntity, Product> {

	@Override
	public Product convert(ProductEntity productEntity) {
		
		if(ObjectUtils.isEmpty(productEntity)) {
			throw new IllegalArgumentException("Cannot convert Null productEntity object to a service object");
		}
		
		Product serviceProduct = new Product();
		
		serviceProduct.setId(productEntity.getId());
		serviceProduct.setName(productEntity.getName());
		serviceProduct.setTimestamp(new DateTime(productEntity.getTimestamp().toInstant().toEpochMilli()));
		serviceProduct.setDescription(productEntity.getDescription());
		serviceProduct.setUnitType(productEntity.getUnitType());
		serviceProduct.setUnitPrice(productEntity.getUnitPrice());
		serviceProduct.setGallery(productEntity.getGallery());
		serviceProduct.setActualAmount(productEntity.getActualAmount());
		serviceProduct.setAvailableAmount(productEntity.getAvailableAmount());
		
		
		return serviceProduct;
	}

}
