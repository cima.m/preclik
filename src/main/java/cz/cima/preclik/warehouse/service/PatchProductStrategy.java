package cz.cima.preclik.warehouse.service;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Component;

import cz.cima.preclik.warehouse.data.model.ProductEntity;

/**
 * merges two provided objects into one in a way that target product will change given 
 * attributes to those non-null attributes from productPatch.
 */
@Component
public class PatchProductStrategy {

	/**
	 * Replaces target attributes by those non-null from product patch
	 * 
	 * @param targetProduct that will have its attributes updated
	 * @param productPatch desired target values of certain target product attributes
	 */
	/* default */ void patch(ProductEntity targetProduct, ProductEntity productPatch) {
		
		targetProduct.setName(ObjectUtils.getIfNull
				(productPatch.getName(), 
				() -> targetProduct.getName()));
		
		targetProduct.setDescription(ObjectUtils.getIfNull
				(productPatch.getDescription(), 
				() -> targetProduct.getDescription()));
		
		targetProduct.setGallery(ObjectUtils.getIfNull
				(productPatch.getGallery(), 
				() -> targetProduct.getGallery()));
		
		targetProduct.setUnitType(ObjectUtils.getIfNull
				(productPatch.getUnitType(), 
				() -> targetProduct.getUnitType()));
		
		targetProduct.setUnitPrice(ObjectUtils.getIfNull
				(productPatch.getUnitPrice(), 
				() -> targetProduct.getUnitPrice()));
		
		targetProduct.setAvailableAmount(ObjectUtils.getIfNull
				(productPatch.getAvailableAmount(), 
				() -> targetProduct.getAvailableAmount()));
		
		targetProduct.setActualAmount(ObjectUtils.getIfNull
				(productPatch.getActualAmount(), 
				() -> targetProduct.getActualAmount()));
		
	}
	
}
