package cz.cima.preclik.warehouse.service.model;

import org.joda.time.DateTime;

/**
 * Service layer representation of product being sold via this web shop.
 */
@SuppressWarnings("PMD.DataClass")
public class Product extends ProductDescription {
	
	private String productId;
    private DateTime timestamp;
    private int actualAmount;
    private int availableAmount;
    
	/**
	 * @return unique identifier of product represented by this object within system.
	 */
	public String getId() {
		return productId;
	}
	
	
	/**
	 * Sets given value as a unique identifier of product represented by this object.
	 * 
	 * @param productId string to be used to uniquely identify product represented by this object within system. 
	 */
	public void setId(String productId) {
		this.productId = productId;
	}
	public DateTime getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(DateTime timestamp) {
		this.timestamp = timestamp;
	}
	public int getActualAmount() {
		return actualAmount;
	}
	public void setActualAmount(int actualAmount) {
		this.actualAmount = actualAmount;
	}
	public int getAvailableAmount() {
		return availableAmount;
	}
	public void setAvailableAmount(int availableAmount) {
		this.availableAmount = availableAmount;
	}
    

}
