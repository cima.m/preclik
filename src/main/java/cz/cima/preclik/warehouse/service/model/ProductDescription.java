package cz.cima.preclik.warehouse.service.model;

/**
 * Service layer representation of product description being sold via this web shop.
 * Used mainly for creation of new product as no state is required.
 */
@SuppressWarnings("PMD.DataClass")
public class ProductDescription {

    private String name;
    private String description;
    private String unitType;
    private double unitPrice;
    private String gallery;
    

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUnitType() {
		return unitType;
	}
	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}
	public double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}
	public String getGallery() {
		return gallery;
	}
	public void setGallery(String gallery) {
		this.gallery = gallery;
	}
}
