package cz.cima.preclik.warehouse.service;

/**
 * An exception telling that there is a temporary problem in reaching the remote product repository.
 */
public class ProductRepositoryException extends Exception {

	private static final long serialVersionUID = -8324562895198343153L;

}
