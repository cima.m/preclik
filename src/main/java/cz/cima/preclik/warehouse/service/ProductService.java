package cz.cima.preclik.warehouse.service;

import java.sql.Timestamp;
import java.time.Clock;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import javax.transaction.Transactional;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import cz.cima.preclik.warehouse.data.ProductRepository;
import cz.cima.preclik.warehouse.data.model.ProductEntity;
import cz.cima.preclik.warehouse.service.model.Product;
import cz.cima.preclik.warehouse.service.model.ProductDescription;

/**
 * Service layer interface to manage Products within this web shop.
 *
 */
@Service
public class ProductService {

	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private ConversionService conversionService;
	
	@Autowired
	private PatchProductStrategy patchStrategy;

	private final Clock clock;
	
	/**
	 * @param clock a reference time source 
	 * to be used for time stamping the database entity.
	 */
	@Autowired
	public ProductService(Clock clock) {
		this.clock = clock;
	}
	
	/**
	 * Creates a new product in system based on provided product description.
	 * 
	 * @param productDescription description of new product how it suppose to be created in system. <strong>Product ID is ignored.</strong>
	 * @return an actual product object how it was created in system. 
	 *         Especially product ID was populated and can be used for further reference.
	 */
	public Product createProduct(ProductDescription productDescription) throws ProductCreationException, MalformedProductException {
		
		if(ObjectUtils.anyNull(productDescription)) {
			throw new MalformedProductException();
		}
		
		final ProductEntity productEntity = conversionService.convert(productDescription, ProductEntity.class);

		productEntity.setId(UUID.randomUUID().toString());
		productEntity.setTimestamp(Timestamp.from(clock.instant()));
		productEntity.setAvailableAmount(0);
		productEntity.setActualAmount(0);
				
		final ProductEntity resultProductEntity = productRepository.save(productEntity);
		
		return conversionService.convert(resultProductEntity, Product.class);
		
	}

	/**
	 * NOTE: this is only a demo implementation and no paging or other optimization is done.
	 * 
	 * @return a collection of all products within system. 
	 * @throws ProductRepositoryException when there was a problem with communication to database. 
	 */
	public Collection<Product> listProducts() throws ProductRepositoryException {
		//TODO catch some JPA exceptions
		Iterable<ProductEntity> allProducts = productRepository.findAll();
		
		final List<Product> productList;
		if(Objects.nonNull(allProducts)) {
			productList = new LinkedList<>();
			for (ProductEntity productEntity : allProducts) {
				productList.add(conversionService.convert(productEntity, Product.class));
			}
		} else {
			productList = Collections.emptyList();
		}
		
		return productList;
	}


	/**
	 * Patches product denoted by ID in provided object with other values in provided object.
	 * 
	 * @param serviceProduct a product representing object where ID must be defined 
	 *        and is immutable in repository. 
	 *        Other attributes can be null while only those non-null will be used 
	 *        to patch the database entity.
	 *        
	 * @return Patched object
	 * @throws MalformedProductException when provided object lacks fully populated Product ID.
	 * @throws NoSuchProductException when product denoted by provided Product id attribute wasn't found in database.
	 */
	@Transactional
	public Product patchProduct(Product serviceProduct) throws MalformedProductException, NoSuchProductException {
		
		if(Objects.isNull(serviceProduct) || StringUtils.isBlank(serviceProduct.getId())) {
			throw new MalformedProductException();
		}
		
		final Optional<ProductEntity> currentProductOpt = productRepository.findById(serviceProduct.getId());
		if(Objects.isNull(currentProductOpt) || ! currentProductOpt.isPresent()) {
			throw new NoSuchProductException();
		}
			
		final ProductEntity entityPatch = conversionService.convert(serviceProduct, ProductEntity.class);
		
		final ProductEntity currentProduct = currentProductOpt.get();
		patchStrategy.patch(currentProduct, entityPatch);
		currentProduct.setTimestamp(Timestamp.from(clock.instant()));
		
		//TODO catch some JPA runtime exceptions
		ProductEntity resultingProduct = productRepository.save(currentProduct);
		
		return conversionService.convert(resultingProduct, Product.class);
	}
	
}
