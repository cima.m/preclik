package cz.cima.preclik.warehouse.service;

/**
 * An Exception indicating that persisting of entity failed.
 */
public class ProductCreationException extends Exception {

	private static final long serialVersionUID = 2345358223996770602L;

}
