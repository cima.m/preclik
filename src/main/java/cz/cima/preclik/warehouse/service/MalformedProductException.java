package cz.cima.preclik.warehouse.service;

/**
 * Exception to indicated that provided object wasn't properly initialized.
 *
 */
public class MalformedProductException extends Exception {

	private static final long serialVersionUID = 2067535406326682946L;

}
