package cz.cima.preclik.warehouse.service;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import cz.cima.preclik.warehouse.data.model.ProductEntity;
import cz.cima.preclik.warehouse.service.model.ProductDescription;

/**
 *Converts Service layer Product Description to a not-fully initialized Product Entiry usable for serialization with JPA.
 */
 @Component
public class ServiceProductDescriptionToProductEntityConverter implements Converter<ProductDescription, ProductEntity> {

	@Override
	public ProductEntity convert(ProductDescription serviceProduct) {
		
		if(ObjectUtils.isEmpty(serviceProduct)) {
			throw new IllegalArgumentException("Cannot convert Null service Product object to entity");
		}
		
		ProductEntity productEntity = new ProductEntity();
		
		productEntity.setName(serviceProduct.getName());
		productEntity.setDescription(serviceProduct.getDescription());
		productEntity.setUnitType(serviceProduct.getUnitType());
		productEntity.setUnitPrice(serviceProduct.getUnitPrice());
		productEntity.setGallery(serviceProduct.getGallery());
		
		return productEntity;
	}
}
