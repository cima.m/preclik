package cz.cima.preclik.warehouse.service;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;

import java.sql.Timestamp;
import java.time.Clock;
import java.time.ZoneId;

import org.mockito.MockitoAnnotations;
import org.springframework.core.convert.ConversionService;

import cz.cima.preclik.warehouse.data.ProductRepository;
import cz.cima.preclik.warehouse.data.model.ProductEntity;
import cz.cima.preclik.warehouse.service.model.ProductDescription;

public class ProductServiceTest {
	
	private static final int PRODUCT_AVAILABLE_AMOUNT = 10;

	private static final int PRODUCT_ACTUAL_AMOUNT = 20;

	private static final double PRODUCT_UNIT_PRICE = 100.0;

	private static final String PRODUCT_UNIT_TYPE = "ks";

	private static final String DEFAULT_PRODUCT_GALLERY = "default-gallery";

	private static final String DEFAULT_PRODUCT_DESCRIPTION = "A default testing product description";

	private static final String DEFAULT_PRODUCT_NAME = "default name";

	private static final String DEFAULT_PRODUCT_ID = "default";

	@Mock
	private ProductRepository productRepository;
	
	@Mock
	private ConversionService conversionService;
	
	private Clock clock = Clock.fixed(Clock.systemUTC().instant(), ZoneId.of("UTC"));
	
	@InjectMocks
	private ProductService productService = new ProductService(clock);
	

	private ProductDescription serviceProduct = initServiceProduct();

	private ProductEntity productEntity = initEntityProduct();
	
	@Captor
	private ArgumentCaptor<ProductEntity> entityCaptor;
	
	@Before
	public void initMocks() {
		MockitoAnnotations.openMocks(this);
		
		when(conversionService.convert(serviceProduct, ProductEntity.class)).thenReturn(productEntity);
	}
	
	private ProductDescription initServiceProduct() {
		
		ProductDescription product = new ProductDescription();
		product.setName(DEFAULT_PRODUCT_NAME);
		product.setDescription(DEFAULT_PRODUCT_DESCRIPTION);
		product.setGallery(DEFAULT_PRODUCT_GALLERY);
		product.setUnitType(PRODUCT_UNIT_TYPE);
		product.setUnitPrice(PRODUCT_UNIT_PRICE);

		return product;
	}
	
	private ProductEntity initEntityProduct() {
		
		ProductEntity productEntity = new ProductEntity();
		productEntity.setId(DEFAULT_PRODUCT_ID);
		productEntity.setName(DEFAULT_PRODUCT_NAME);
		productEntity.setDescription(DEFAULT_PRODUCT_DESCRIPTION);
		productEntity.setTimestamp(Timestamp.valueOf("2020-01-01 01:02:03"));
		productEntity.setGallery(DEFAULT_PRODUCT_GALLERY);
		productEntity.setUnitType(PRODUCT_UNIT_TYPE);
		productEntity.setUnitPrice(PRODUCT_UNIT_PRICE);
		
		productEntity.setActualAmount(PRODUCT_ACTUAL_AMOUNT);
		productEntity.setAvailableAmount(PRODUCT_AVAILABLE_AMOUNT);	
		
		return productEntity;
	}

	@Test(expected = MalformedProductException.class)
	public void createProductIsNullSafe() throws ProductCreationException, MalformedProductException {
		productService.createProduct(null);
	}
	
	
	@Test
	public void generateId() throws ProductCreationException, MalformedProductException {
		productService.createProduct(serviceProduct);
		
		verify(conversionService).convert(serviceProduct, ProductEntity.class);
		
		verify(productRepository).save(entityCaptor.capture());
		ProductEntity storedEntity = entityCaptor.getValue();

		
		//Amounts must be reset
		assertEquals(0, storedEntity.getActualAmount());
		assertEquals(0, storedEntity.getAvailableAmount());
		
		//Timestamp must be now
		assertEquals(clock.instant(), storedEntity.getTimestamp().toInstant());
				
		//Gallery passes through
		assertEquals(serviceProduct.getGallery(), storedEntity.getGallery());
		
		//Name passes through
		assertEquals(serviceProduct.getName(), storedEntity.getName());
		
		//Description passes through
		assertEquals(serviceProduct.getDescription(), storedEntity.getDescription());
		
		//Unit type passes through
		assertEquals(serviceProduct.getUnitType(), storedEntity.getUnitType());
		
		//Unit price passes through
		assertEquals(serviceProduct.getUnitPrice(), storedEntity.getUnitPrice(), 0.01);
		
		
	}
	
	@Test
	public void cleanupAmounts() {
		
	}
	
	@Test
	public void returnResultingEntry() {
		
	}
}
