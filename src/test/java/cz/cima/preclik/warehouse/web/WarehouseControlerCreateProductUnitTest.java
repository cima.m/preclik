package cz.cima.preclik.warehouse.web;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;

import static org.mockito.Mockito.*;

import cz.cima.preclik.warehouse.service.MalformedProductException;
import cz.cima.preclik.warehouse.service.ProductCreationException;
import cz.cima.preclik.warehouse.service.ProductService;
import static org.junit.Assert.*;

public class WarehouseControlerCreateProductUnitTest {

	@Mock
	private ProductService productService;
	
	@Mock
	private WebProductToServiceProductConverter productFactory;
		
	@Mock
	private ConversionService conversionService;
	
	@InjectMocks
	private WarehouseControler controller;
	
	cz.cima.preclik.warehouse.web.model.ProductDescription webProduct = new cz.cima.preclik.warehouse.web.model.ProductDescription();
	cz.cima.preclik.warehouse.web.model.Product newWebProduct = new cz.cima.preclik.warehouse.web.model.Product();
	cz.cima.preclik.warehouse.service.model.ProductDescription serviceProduct = new cz.cima.preclik.warehouse.service.model.ProductDescription();
	cz.cima.preclik.warehouse.service.model.Product newServiceProduct = new cz.cima.preclik.warehouse.service.model.Product();
	
	@Before
	public void initMocks() {
		MockitoAnnotations.openMocks(this);
	}
	
	@Test
	public void convertAndForwardProduct() throws ProductCreationException, MalformedProductException {


		
		when(conversionService.convert(any(), eq(cz.cima.preclik.warehouse.service.model.ProductDescription.class))).thenReturn(serviceProduct);
		
		when(productService.createProduct(any())).thenReturn(newServiceProduct);
		
		when(conversionService.convert(any(), eq(cz.cima.preclik.warehouse.web.model.Product.class))).thenReturn(newWebProduct);
		
		var response = controller.createProduct(webProduct);
		
		verify(conversionService).convert(webProduct, cz.cima.preclik.warehouse.service.model.ProductDescription.class);
		
		verify(productService).createProduct(serviceProduct);
		
		verify(conversionService).convert(newServiceProduct, cz.cima.preclik.warehouse.web.model.Product.class);
		
		assertEquals(newWebProduct, response.getBody());
		assertEquals(HttpStatus.OK, response.getStatusCode());
		
	}
	
	@Test
	public void return400onRejectedProduct() throws ProductCreationException, MalformedProductException {

		assertExceptionConversion(new MalformedProductException(), HttpStatus.BAD_REQUEST);
		
	}
	
	@Test
	public void return500onPersistencefailure() throws ProductCreationException, MalformedProductException {

		assertExceptionConversion(new ProductCreationException(), HttpStatus.INTERNAL_SERVER_ERROR);
		
	}
	
	private void assertExceptionConversion(Throwable exception, HttpStatus expectedStatus) throws ProductCreationException, MalformedProductException {
		when(conversionService.convert(any(), eq(cz.cima.preclik.warehouse.service.model.ProductDescription.class))).thenReturn(serviceProduct);
		
		when(productService.createProduct(any())).thenThrow(exception);
		
		var response = controller.createProduct(webProduct);
		
		verify(conversionService).convert(webProduct, cz.cima.preclik.warehouse.service.model.ProductDescription.class);
		
		verify(productService).createProduct(serviceProduct);
		
		verify(conversionService, never()).convert(any(), eq(cz.cima.preclik.warehouse.web.model.Product.class));

		assertEquals(expectedStatus, response.getStatusCode());	
	}
}
