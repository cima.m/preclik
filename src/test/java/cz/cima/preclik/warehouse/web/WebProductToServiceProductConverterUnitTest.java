package cz.cima.preclik.warehouse.web;

import static org.junit.Assert.*;
import org.junit.Test;

import cz.cima.preclik.warehouse.web.model.Product;

public class WebProductToServiceProductConverterUnitTest {

	WebProductToServiceProductConverter converter = new WebProductToServiceProductConverter();
	
	@Test
	public void isNullSafe() {
		
		Product webProduct = new Product();
		
		var serviceProduct = converter.convert(webProduct);
		
		assertEquals(webProduct.getId(), serviceProduct.getId());
		assertEquals(webProduct.getName(), serviceProduct.getName());
		assertEquals(webProduct.getDescription(), serviceProduct.getDescription());
		assertEquals(webProduct.getGallery(), serviceProduct.getGallery());
		
		assertEquals(0.0, serviceProduct.getUnitPrice(), 0.001);
		
		assertEquals(webProduct.getUnitType(), serviceProduct.getUnitType());
		
		
		assertEquals(0, serviceProduct.getActualAmount());
		assertEquals(0, serviceProduct.getAvailableAmount());
		
		assertEquals(webProduct.getTimestamp(), serviceProduct.getTimestamp());
		
	}
}
